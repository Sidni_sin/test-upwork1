<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171025_103533_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id'        => $this->primaryKey(),
            'username'  => $this->string()->notNull(),
            'password'  => $this->string()->notNull(),
            'balance'   => $this->double(2)->notNull()->defaultValue(0),
            'auth_key'  => $this->string()->notNull()->defaultValue('i love linux'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
