<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translations`.
 */
class m171026_101721_create_translations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('translations', [
            'id'                => $this->primaryKey(),
            'username'          => $this->string()->notNull(),
            'recipient_username'=> $this->string()->notNull(),
            'pay'               => $this->double(2)->notNull(),
            'date'              => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('translations');
    }
}
