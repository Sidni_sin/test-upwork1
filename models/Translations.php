<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Translations extends ActiveRecord
{

    public static function tableName()
    {
        return 'translations';
    }

    public function getPays()
    {
        $username = Yii::$app->user->identity->username;

        $res = $this->find()
                    ->where(['username' => $username])
                    ->orWhere(['recipient_username' => $username])
                    ->asArray()
                    ->all();

        return $res;
    }

    public function addPay()
    {
        $this->username = Yii::$app->user->identity->username;
        $this->date = date('Y-m-d H:i:s');

        $res = false;

        if($this->save()){
            $res = true;
        }

        return $res;
    }

    public function addMoneyUser()
    {

        $user_recipient = User::findByUsername($this->recipient_username);

        $user_recipient['balance'] += $this->pay;

        $res = false;

        if($user_recipient->save()){
            $res = true;
        }

        return $res;
    }

    public function moneyThisUser()
    {

        $user = User::findIdentity(Yii::$app->user->identity->id);

        $user['balance'] -= $this->pay;

        $res = false;

        if($user->save()){
            $res = true;
        }

        return $res;
    }

    public function rules()
    {
        return [

            [['username', 'recipient_username','pay', 'date'], 'required'],

            ['pay', 'double', 'min' => 1],

        ];
    }


}