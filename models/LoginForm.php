<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends ActiveRecord
{

    public $rememberMe = true;

    private $_user = false;

    const SCENARIO_LOGIN        = 'login';
    const SCENARIO_LOGIN_SAVE   = 'login_save';
    const SCENARIO_ADD_USER     = 'add_user';

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['username', 'password'], 'required',  'on' => self::SCENARIO_LOGIN],
            ['rememberMe', 'boolean',               'on' => self::SCENARIO_LOGIN],
            ['password', 'validatePassword',        'on' => self::SCENARIO_LOGIN],


            ['username', 'unique',                  'on' => self::SCENARIO_LOGIN_SAVE],
            [['username', 'password'], 'required',  'on' => self::SCENARIO_LOGIN_SAVE],
            ['rememberMe', 'boolean',               'on' => self::SCENARIO_LOGIN_SAVE],
            ['balance', 'default', 'value' => 0,    'on' => self::SCENARIO_LOGIN_SAVE],

        ];
    }

    public function attributeLabels()
    {
        return [
            'username'      => 'Логин',
            'password'      => 'Пароль',
            'rememberMe'    => 'Запомнить',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect login or password.');
            }
        }
    }

    /**
     * @param $pas
     * @return string 60 ASCII characters
     */
    public function createHasPassword($pas)
    {
        $hash = Yii::$app->getSecurity()->generatePasswordHash($pas);
        return $hash;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $res = false;

        if ($this->validate()) {
            $res = $this->loginTrue();
        }

        return $res;
    }

    public function loginTrue()
    {
        if ($this->rememberMe) {
            $u = $this->getUser();
            $u->generateAuthKey();
            $u->save();
        }

        return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
