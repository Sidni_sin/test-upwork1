<?php


$session = Yii::$app->session;

?>

    <div class="row">

        <div class="col-sm-12">
            <h1 class="page-header">My room</h1>

                <?php if($session->hasFlash('error')): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $session->getFlash('error') ?>
                    </div>
                <?php endif; ?>

            <div class="page-header">My balance: <b><?= Yii::$app->user->identity->balance ?></b></div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>username</th>
                            <th>recipient username</th>
                            <th>pay</th>
                            <th>date</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pays as $item): ?>
                            <tr>
                                <td><?= $item['id'] ?></td>
                                <td><?= $item['username'] ?></td>
                                <td><?= $item['recipient_username'] ?></td>
                                <td><?= $item['pay'] ?></td>
                                <td><?= $item['date'] ?></td>
                                <td><?= ($item['username'] == Yii::$app->user->identity->username) ? 'I paid' : 'I got' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
