<?php

namespace app\controllers;


use app\models\LoginForm;
use app\models\Translations;
use app\models\User;
use app\models\Users;
use Yii;
use yii\helpers\Url;

class RoomController extends AppController
{

    public function actionIndex()
    {
        $transl = new Translations();

        $pays = $transl->getPays();

        return $this->render('index', compact('pays'));
    }

    public function actionPay()
    {
        $model = new Translations();

        return $this->render('pay', compact('model'));

    }

    public function actionAddPay()
    {
        if(Yii::$app->request->isPost){
            $session = Yii::$app->session;
            $post    = Yii::$app->request->post();
            $transl  = new Translations();
            $transl->load($post);
            if($transl->recipient_username != Yii::$app->user->identity->username){

                $user_recipient = User::findByUsername($transl->recipient_username);
                $transaction    = $transl->getDb()->beginTransaction();

                //transaction
                try{
                    //add user
                    if(empty($user_recipient)){

                        $login = new LoginForm(['scenario' => LoginForm::SCENARIO_LOGIN_SAVE]);

                        $login_data['LoginForm']['username']    = $transl->recipient_username;
                        $login_data['LoginForm']['password']    = $login->createHasPassword($transl->recipient_username);

                        $login->load($login_data);

                        if(!$login->save()){
                            $session->setFlash('error', array_shift(array_shift($login->errors)),false);
                        }

                    }
                    //add pay
                    if(!$transl->addPay() || !$transl->addMoneyUser() || !$transl->moneyThisUser()){

                        if(empty($transl->errors)){
                            $session->setFlash('error','Sorry, error',false);
                        }else{
                            $session->setFlash('error', array_shift(array_shift($transl->errors)),false);
                        }

                    }

                    //If the error rolls back db
                    if(!$session->hasFlash('error')){
                        $transaction->commit();
                    }

                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }

            }else{
                $session->setFlash('error','You can not send to yourself',false);
            }

        }

        return $this->redirect(['/']);

    }


}